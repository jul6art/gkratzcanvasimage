/**
 * @param {HTMLImageElement} image The Image
 * @param {HTMLCanvasElement} canvas The Canvas
 */
var GKratzCanvas = function (image, canvas) {
    this.image = document.getElementById(image.attr('id'));
    this.canvas = document.getElementById(canvas.attr('id'));

    this.setSize = function () {
        this.canvas.height = this.image.height;
        this.canvas.width = this.image.width;
    };

    this.getCanvas = function (id) {
        return this.canvas;
    };

    this.getImage = function (id) {
        return this.image;
    };

    this.init = function () {
        this.context = this.canvas.getContext('2d');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);

        $('body').trigger('gci_init', [$(this.image), $(this.canvas)] );
    };

    this.applyFilter = function (filter, value = false) {
        switch (filter) {
            case 'reset':
                this.reset();
                break;
            case 'grayscale':
                this.grayscale(value);
                break;
            case 'sepia':
                this.sepia(value);
                break;
            case 'invert':
                this.invert(value);
                break;
            case 'blur':
                this.blur(value);
                break;
            case 'brightness':
                this.brightness(value);
                break;
            case 'contrast':
                this.contrast(value);
                break;
            case 'opacity':
                this.opacity(value);
                break;
            case 'saturate':
                this.saturate(value);
                break;
            case 'hue-rotate':
                this.hue(value);
                break;
            case 'red':
                this.paint(0, value);
                break;
            case 'green':
                this.paint(1, value);
                break;
            case 'blue':
                this.paint(2, value);
                break;
            default:
                this.context.filter = filter;
                this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
                break;
        }

        $('body').trigger('gci_filter', [$(this.image), $(this.canvas), filter, value] );
    };

    this.reset = function () {
        this.context.filter = 'none';
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.parseValue = function (value = false) {
        value = parseInt(value);
        if (value >= 0 && value <= 100) {
            return value;
        }
        return 100;
    };

    this.addFilter = function(filter) {
        this.context.filter = filter;
    };

    this.grayscale = function (value = false) {
        this.addFilter('grayscale(' + this.parseValue(value) + '%)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.sepia = function (value = false) {
        this.addFilter('sepia(' + this.parseValue(value) + '%)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.invert = function (value = false) {
        this.addFilter('invert(' + this.parseValue(value) + '%)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.blur = function (value = false) {
        this.addFilter('blur(' + parseInt(this.parseValue(value) / 5) + 'px)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.brightness = function (value = false) {
        this.addFilter('brightness(' + (this.parseValue(value) / 20) + ')');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.contrast = function (value = false) {
        this.addFilter('contrast(' + parseInt(this.parseValue(value) * 5) + '%)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.opacity = function (value = false) {
        this.addFilter('opacity(' + this.parseValue(value) + '%)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.saturate = function (value = false) {
        this.addFilter('saturate(' + parseInt(this.parseValue(value) * 5) + '%)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.hue = function (value = false) {
        this.addFilter('hue-rotate(' + parseInt(this.parseValue(value) * 3.6) + 'deg)');
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
    };

    this.paint = function (index, value = false) {
        this.context.drawImage(this.image, 0, 0, this.image.width, this.image.height);
        var data = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
        for (var i = index, length = data.data.length; i < length; i += 4) {
            data.data[i] = Math.max((2.55 * this.parseValue(value)), data.data[i]);
        }
        this.context.putImageData(data, 0, 0);
    };

    this.setSize();
    this.init();
};