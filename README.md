You can see a demo of the plugin [here](https://vsweb.be/demo/gkratz-canvas-image/demo.html)

# Requirements

    jQuery

# Install

via npm

    npm i gkratz-canvas-image
    
via yarn

    yarn add gkratz-canvas-image
    
# Usage

    <div class="image-container">
        <img src="assets/demo.jpg" alt="" id="demo_img">
        <canvas id="demo_canvas"></canvas>
    </div>
    
    <script src="assets/jquery/dist/jquery.min.js"></script>
    <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="dist/js/gkratz-canvas-image.js"></script>
    <script>
        $(document).ready(function () {
            canvas = new GKratzCanvas($('#demo_image'), $('#demo_canvas'));

            $('your button').on('click', function(e){
                e.preventDefault();
                input = $('your input');
                input.val(100);
                canvas.applyFilter('the filter name', input.val());
            });

            $('your input').on('change', function(e) {
                e.preventDefault();
                canvas.applyFilter('the filter name', $(this).val());
            });
        });
    </script>
    
# Filters

    grayscale
    sepia
    blur
    brightness
    contrast
    invert
    saturate
    hue-rotate
    red
    blue
    green
    
# Save

    canvas.getCanvas().toBlob(function(blob) {
        // Blob is an image file
        
        // send the blob to server to save
        
        // $.ajax({
        //     url : YOUR_PATH,
        //     data: {
        //         image: blob
        //     }
        // });
    });
    
# Reset

To reset the canvas, you nee to apply the reset Filter

    canvas.reset();
    
# Events

init (warning, listener mut be declare before canvas initialisation)

    $('body').on('gci_init', function (event, image, canvas) {
        console.log(event);
        console.log(image);
        console.log(canvas);
    });
    
    canvas = new GKratzCanvas(..., ...);

apply filter

    $('body').on('gci_filter', function (event, image, canvas, filter, value) {
        console.log(event);
        console.log(image);
        console.log(canvas);
        console.log(filter);
        console.log(value);
    });
    